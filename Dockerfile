FROM silex/emacs:28-alpine-ci
LABEL maintainer="Rodrigo J. da Fonseca <rodrigo.fonseca@arquivei.com.br>"

RUN apk --no-cache upgrade && apk add --no-cache bash ripgrep fd

RUN adduser emacs -D
USER emacs
WORKDIR /home/emacs

RUN git clone --depth 1 https://github.com/doomemacs/doomemacs /home/emacs/.emacs.d
ENV HOME=/home/emacs
ENV PATH=/home/emacs/.emacs.d/bin:$PATH

COPY doom.d /home/emacs/.doom.d

RUN yes | doom install

ENTRYPOINT /bin/bash
