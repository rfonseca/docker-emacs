##
# Docker Emacs
#

SHELL := bash

.PHONY: image
image: ## Builds the basic image
	docker build -t rjfonseca/emacs:latest .

.PHONY: push
push: ## push sends the image to docker hub at https://hub.docker.com/repository/docker/rjfonseca/emacs/general
	docker push rjfonseca/emacs:latest
